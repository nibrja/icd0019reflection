package samples;

public class Deprecation {

    public static void main(String[] args) throws Exception {

        new SomeOtherClass().someOldMethod();

        Deprecation.class.newInstance();
    }

}

class SomeOtherClass {

    /**
     * @Deprecated use newAndBetterMethodForTheSameTask() instead.
     */
    @Deprecated
    public void someOldMethod() {

    }
    public void newAndBetterMethodForTheSameTask() {

    }
}