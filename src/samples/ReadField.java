package samples;

import java.lang.reflect.Field;

public class ReadField {

    private String greeting = "hello";

    public static void main(String[] args) throws Exception {

        Field field = ReadField.class.getDeclaredField("greeting");

        field.setAccessible(true);

        Object value = field.get(new ReadField());

        System.out.println(value);

    }
}
