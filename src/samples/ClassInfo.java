package samples;

public class ClassInfo {

    public static void main(String[] args) {

        ClassInfo instance = new ClassInfo();

        Class<? extends ClassInfo> clazz = instance.getClass();

        System.out.println(clazz.getName());
        System.out.println(clazz.getSimpleName());
        System.out.println(clazz.getPackageName());

        System.out.println(ClassInfo.class.getName());
    }

}
