package samples;

public class Create {

    public static void main(String[] args) throws Exception {

        Class<Create> clazz = Create.class;

        Create instance = clazz.getDeclaredConstructor().newInstance();

        instance.hello();
    }

    private void hello() {
        System.out.println("hello");
    }

}
