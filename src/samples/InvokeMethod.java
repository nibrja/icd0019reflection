package samples;

import java.lang.reflect.Method;

public class InvokeMethod {

    public static void main(String[] args) throws Exception {

        Method method = InvokeMethod.class.getDeclaredMethod("hello");

        method.invoke(new InvokeMethod());

    }

    private void hello() {
        System.out.println("hello");
    }

}
